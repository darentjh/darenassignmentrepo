/**
 * Created by Daren on 28/3/16.
 */
var express = require("express");
/* this just says that I want to use the express application*/
var app = express();
/* this means from express, we want to create an application*/

app.get("/", function(req, res) {
    /* this says that whenever someone hits "/" , we run the function*/
        res.status(202);
        //this refers to the status, MUST ALWAYS HAVE STATUS CODE AND MIME TYPE
        res.type("text/plain");
        /*we are not sending back html, we are sending back plain text, refer to MIME type WIKI*/
        res.send("The current time is " + (new Date()));
    }
);


app.get("/hello", function(req, res) {
    res.status(202);
    res.type("text/html");
    res.send("<h1><strong>HELLO</strong></h1>");
    }
);

app.get("/image", function(req, res) {
        res.status(202);
        res.type("image/jpeg");
        res.send("<h1><strong>HELLO</strong></h1>");
    }
);

app.listen(3000, function(){
    console.info("Application started on port 3000");
    //3000 - port number; means start this application on port 3000. port refers to the unit number in HDB. IP refers to block number
    }
);